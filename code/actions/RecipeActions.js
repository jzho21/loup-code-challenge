import {FETCH_RECIPES_REQUEST} from '../constants/actions-types'

/**
 * Actions for fetching recipe data
 */
export const fetchRecipeData = () => {
  return {
    type: FETCH_RECIPES_REQUEST,
  }
};
