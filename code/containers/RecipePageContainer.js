import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {fetchRecipeData} from '../actions/RecipeActions'
import RecipePage from '../components/pages/RecipePage'

class RecipePageContainer extends Component{
  componentDidMount() {
    this.props.fetchRecipes()
  }

  render(){
    return (<RecipePage {...this.props} />)
  }
}

const mapStateToProps = (state) => ({
  recipes: state.RecipeReducers.recipes || [],
})

const mapDispatchToProps = (dispatch) => ({
  fetchRecipes: ()=>dispatch(fetchRecipeData())
})

RecipePageContainer.propTypes = {
  recipes: PropTypes.array.isRequired,
  fetchRecipes: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(RecipePageContainer)
