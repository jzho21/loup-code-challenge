import axios from 'axios'
import {BASE_SURVEYS_ENDPOINT} from '../../constants/endpoints'

export const FETCH_RECIPES_FUNC = () => {
  return axios.get(BASE_SURVEYS_ENDPOINT).then((response)=>{
    if(response && response.data){
      return response.data.contents
    }
  })
}
