import React from 'react'
import styled from 'styled-components'
import {palette, font} from 'styled-theme'

/**
 * Give a standard block style for the page
 */
const StyledBlock = styled.div`
  color: ${palette('primary', 1)}
  font-family: ${font('primary')};
  font-size: .6rem;
  line-height: 2rem;
  background-color: white;
`

const Block = ({ ...props }) => {
  return (
    <StyledBlock {...props} />
  )
};

export default Block

