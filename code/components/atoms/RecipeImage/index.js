import React from 'react'
import styled from 'styled-components'

const RecipeImageWrapper = styled.img`
  object-fit: cover;
  width: 300px;
  height: 200px;
`

const RecipeImage = ({ ...props }) => {
  return (
    <RecipeImageWrapper  {...props} />
  )
};

export default RecipeImage

