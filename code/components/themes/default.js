/**
 * Define the basic theme for the pages
 *
 * @type {{}}
 */

const theme = {}

theme.palette = {
  primary: ['#1a1a1a', '#3d3939', '#2196f3', '#71bcf7', '#c2e2fb'],
  secondary: ['#c2185b', '#e91e63', '#f06292', '#f8bbd0'],
  danger: ['#d32f2f', '#f44336', '#f8877f', '#ffcdd2'],
  alert: ['#ffa000', '#ffc107', '#ffd761', '#ffecb3'],
  success: ['#388e3c', '#4caf50', '#7cc47f', '#c8e6c9'],
  white: ['#fff', '#fff', '#eee'],
  chart: ['#125448'],
  grayscale: [
    '#212121',
    '#414141',
    '#616161',
    '#9e9e9e',
    '#bdbdbd',
    '#e0e0e0',
    '#eeeeee',
    '#ffffff',
  ],
}

theme.fonts = {
  primary: 'Helvetica Neue, Helvetica, Roboto, sans-serif',
}

const size = {
  mobile: '320px',
  tablet: '768px',
  laptop: '1440px',
}

export const device = {
  mobileS: `(min-width: ${size.mobile})`,
  tablet: `(min-width: ${size.tablet})`,
  desktop: `(min-width: ${size.laptop})`,
};

export default theme
