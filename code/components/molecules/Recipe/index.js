/**
 * A recipe is consider as molecules component, as it contains one block component and an image
 */
import React from 'react'
import Block from '../../atoms/Block'
import RecipeImage from '../../atoms/RecipeImage'
import styled from 'styled-components'
import PropTypes from "prop-types";

const RecipeWrapper = styled.div`
  display: inline-block;
  flex-direction: column;
  text-transform: uppercase;
  background-color: white;
  text-align: center;
`

const Recipe = (props) => {
  const {title, imageList} = props
  const imageUrl = imageList.landscape32medium3x.url
  return (
    <RecipeWrapper>
      <RecipeImage src={imageUrl} />
      <Block>{title}</Block>
    </RecipeWrapper>
  )
}

Recipe.propTypes = {
  title: PropTypes.string.isRequired,
  imageList: PropTypes.object,
}


export default Recipe
