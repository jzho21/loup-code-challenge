import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  box-sizing: border-box;
`

const Content = styled.section`
  max-width: 1024px;
  box-sizing: border-box;
  margin: 10px auto;
  max-width: ${props => props.maxWidth}px;
  background-color: #d4d4d4;
`

/**
 * Give a generic layout for the page.
 *
 * @param children
 * @param props
 * @returns {*}
 * @constructor
 */
const GenericTemplate = ({ children, ...props }) => {
  const {maxWidth} = props
  return (
    <Wrapper {...props}>
      <Content maxWidth={maxWidth}>{children}</Content>
    </Wrapper>
  )
}

GenericTemplate.propTypes = {
  children: PropTypes.any.isRequired,
  maxWidth: PropTypes.number.isRequired,
}

GenericTemplate.defaultProps = {
  maxWidth: window.innerWidth - window.innerWidth * 0.08
}

export default GenericTemplate
