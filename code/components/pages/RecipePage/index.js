import React from "react";
import GenericTemplate from "../../templates/GenericTemplate";
import Recipes from "../../organisms/Recipes"
import PropTypes from "prop-types";

const RecipePage = (props) => {
  return(
    <GenericTemplate>
      <Recipes {...props} />
    </GenericTemplate>
  )
}

RecipePage.propTypes = {
  recipes: PropTypes.array.isRequired,
}

export default RecipePage
