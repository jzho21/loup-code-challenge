import React from 'react';
import { Switch, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import theme from '../components/themes/default'
import RecipePageContainer from '../containers/RecipePageContainer'

/**
 * Render the app
 * @returns {*}
 * @constructor
 */
const App = () => (
  <div>
    <ThemeProvider theme={theme}>
      <Switch>
        <Route path="/" component={RecipePageContainer} exact />
      </Switch>
    </ThemeProvider>
  </div>
);

export default App
