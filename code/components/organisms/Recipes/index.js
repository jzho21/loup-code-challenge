/**
 * Recipes contain may recipe component
 */

import React from 'react'
import styled from 'styled-components'
import Recipe from '../../molecules/Recipe'
import {device} from '../../themes/default'
import PropTypes from "prop-types";

const RecipesWrapper = styled.div`
  display: grid;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 1fr;
  margin: .5em;
  
  @media ${device.tablet} {  
    grid-template-columns: 1fr 1fr;
  }
  
  @media ${device.desktop} {  
    grid-template-columns: 1fr 1fr 1fr;
  } 
`

const Recipes = (props) => {
  const {recipes} = props
  return (
    <RecipesWrapper>
      {
        recipes.map((recipe, index)=>{
          return (<Recipe {...recipe} key={index}/>)
        })
      }
    </RecipesWrapper>
  )
}

Recipes.propTypes = {
  recipes: PropTypes.array.isRequired,
}

export default Recipes
