import {FETCH_RECIPES_RESPONSE} from '../constants/actions-types'

const initialState = {}
const Recipes = (state = initialState, {type, payload}) => {
  switch(type) {
    case FETCH_RECIPES_RESPONSE:
      return {...state, recipes: payload}

    default:
      return state
  }
}

export default Recipes
