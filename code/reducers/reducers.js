import { combineReducers } from 'redux';
import RecipeReducers from './RecipeReducers'

export default combineReducers({
  RecipeReducers,
});
