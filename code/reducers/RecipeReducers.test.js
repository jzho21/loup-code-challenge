import 'babel-polyfill'
import RecipeReducers from './RecipeReducers'
import {FETCH_RECIPES_RESPONSE} from '../constants/actions-types'

describe('Test reducer', ()=>{

  it('Test fetch recipe response event', ()=>{
    const response = {ok: true}
    const mockRecipeData = {"contents": ""}

    const SuccessFetchedRecipeData = {
      type: FETCH_RECIPES_RESPONSE,
      payload: mockRecipeData
    };

    const result = RecipeReducers({}, SuccessFetchedRecipeData)
    expect(result.recipes).toBe(mockRecipeData)

  })

})
