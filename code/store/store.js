import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import reducers from '../reducers/reducers';
import rootSaga from '../sagas'

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

const enhancers = [
  applyMiddleware(sagaMiddleware),
  composeWithDevTools(),
]

// Create store
const store = createStore(reducers, compose(...enhancers));
sagaMiddleware.run(rootSaga)

export default store;
