import {call, put, takeEvery} from 'redux-saga/effects'
import {
  FETCH_RECIPES_REQUEST,
  FETCH_RECIPES_RESPONSE,
} from '../constants/actions-types'

import {FETCH_RECIPES_FUNC} from '../services/api'

export function* fetchRecipes(){
  // process the async call to fetch the data
  const recipes = yield call(FETCH_RECIPES_FUNC)
  yield put({type: FETCH_RECIPES_RESPONSE, payload: recipes})
}

export default function* (){
  // Intercept the action.
  yield takeEvery(FETCH_RECIPES_REQUEST, fetchRecipes)
}
