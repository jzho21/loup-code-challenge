import {fork, all} from 'redux-saga/effects'
import RecipeSagas from './RecipeSagas'

export default function* rootSaga(){
  yield all([
    fork(RecipeSagas)
  ])
}
