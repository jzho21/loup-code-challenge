## Pattern and technology
- Atomic React, which is building the component from small to big. 
It can produce highly reusable and consistent style components.
- Saga have been used to load recipe data asynchronisely, because it's easier to read and managing.


## What has been cover
- Using React to build the webapp.
- Using Redux to managing the state.
- Mobile first pattern being for laying out the page component.
- Unit test cover one of reducer as demonstration purpose.

## What can be improved.
- More unit tests can be put into the repo.
- Supporting more device view ports 
- Functional tests will be beneficial.
- Code splitting to improve the performance. The bundle it's quite big now, it definitely  will help
on code splitting. 
- Using different image base on device type instead of hard coding one of them.

## How to run
- `npm i` to install dependency
- `npm start` to run the app in dev environment.
- `npm test` to run unit test.

## Software version dependency
- node v9.11.1
- npm v5.8.0
